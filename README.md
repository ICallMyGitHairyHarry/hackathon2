Blender — сайт по поиску и продвижению игрового видео контента.

Краткое описание идеи: Сайт предоставляет поиск и рекомендации в зависимости от предпочтений конкретного пользователя. При этом можно настроить множество параметров предпочтений: тип видео (стрим, летсплей, нарезка, монтаж и т.д), типаж создателя контента, частоту выхода контента, его длительность, игры и черты создателя контента (голос, чувство юмора и т.д.).
После настройки всех параметров система будет регулярно подбирать для пользователя подходящий контент.

В рамках хакатона Bonch.Hack 2019 были реализованы основные страницы сайта на Vue/Quasar. 

Презентация проекта на хакатоне и сертификат: https://drive.google.com/drive/folders/1NRUJm8ghrRmieWYpXw-r-f3unbB-IQEE?usp=sharing


# h (h)

A Quasar Framework app

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
