const state = {
  categories: ['Игра', 'Категория', 'Харизма', 'Голос', 'Адекватность', 'Оборудование', 'Общительность']
}

const getters = {
  categories: state => state.categories
}

const actions = {
  addCategory (context, data) {
    context.commit('ADD_CATEGORY', data)
  }
}

const mutations = {
  ADD_CATEGORY (state, data) {
    state.categories.push(data)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
